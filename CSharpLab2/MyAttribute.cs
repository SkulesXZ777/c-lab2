﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab2
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    public class MyAttribute  : Attribute
    {
        public int Count { get; set; }
        public MyAttribute(int n)
        {
            Count = n;
        }
    }
}
