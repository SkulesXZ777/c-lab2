﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace CSharpLab2
{
    public partial class Form1 : Form
    {
        private Assembly assembly;

        public Form1()
        {
            InitializeComponent();
        }

        //Получение пути к сборке через OpenFileDialog
        private string selectAssemblyFile()
        {
            openFileDialog1.Filter = @"Dll files (*.dll)|*.dll|Exe files(*.exe)|*.exe|All files|*.*";
            openFileDialog1.Title = @"Select assembly file";
            return (openFileDialog1.ShowDialog() == DialogResult.OK) ? openFileDialog1.FileName : null;
        }

        //Загрузка сборки
        private Assembly openAssembly(string path)
        {
            try
            {
                Assembly a = Assembly.LoadFrom(path);
                return a;
            }
            catch (Exception e)
            {
                MessageBox.Show("Не удалось загрузить указанную сборку!" + e,"Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        //Добавить все классы и интерфейсы сборки к узлу дерева
        void addRoot(TreeNode root, Type[] types)
        {
            TreeNode node;
            foreach (Type type in types)
            {
                node = new TreeNode();
                node.Text = type.ToString();
                //Если класс
                if (type.IsClass)
                {
                    node.ImageIndex = 1;
                    node.SelectedImageIndex = 1;
                    addFirstLevel(node, type);
                    root.Nodes.Add(node);
                }
                //Если интерфейс
                else if (type.IsInterface)
                {
                    node.ImageIndex = 2;
                    node.SelectedImageIndex = 2;
                    addFirstLevel(node, type);
                    root.Nodes.Add(node);
                }
                //Eсли структура или перечесление
                else if (type.IsValueType)
                {
                    if (type.IsEnum)
                    {
                        node.ImageIndex = 10;
                        node.SelectedImageIndex = 10;
                        node.Tag = "Тип: перечисление\n" + type.Attributes + "\n";
                    }
                    else
                    {
                        node.ImageIndex = 11;
                        node.SelectedImageIndex = 11;
                        node.Tag = "Тип: структура\n" + type.Attributes + "\n";
                    }
                    addFirstLevel(node, type);
                    root.Nodes.Add(node);
                }
            }
        }

        //Загрузить все поля, конструкторы и методы
        private void addFirstLevel(TreeNode node, Type type)
        {
            TreeNode node1;
            FieldInfo[] fields = type.GetFields();
            FieldInfo[] fields1 = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo[] fieldsAll = fields.Concat(fields1).ToArray();

            MethodInfo[] methods = type.GetMethods();
            MethodInfo[] methods1 = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);
            MethodInfo[] methodsAll = methods.Concat(methods1).ToArray();

            ConstructorInfo[] constructors = type.GetConstructors();


            //Загрузить поля
            foreach (FieldInfo field in fieldsAll)
            {
                node1 = new TreeNode();
                node1.Text = field.FieldType.Name + " " + field.Name;

                if (field.IsPublic)
                {
                    node1.ImageIndex = 3;
                    node1.SelectedImageIndex = 3;
                }
                else if (field.IsPrivate)
                {
                    node1.ImageIndex = 5;
                    node1.SelectedImageIndex = 5;
                }
                else
                {
                    node1.ImageIndex = 4;
                    node1.SelectedImageIndex = 4;
                }

                string tag = "Тип: поле\n";
                tag += "Тип поля: " + field.FieldType + "\n";
                tag += "Атрибуты: " + field.Attributes;
                node1.Tag = tag;
                node.Nodes.Add(node1);
            }
            //Загрузить конструкторы
            foreach (ConstructorInfo constructor in constructors)
            {
                string s = "", tag = "Тип: конструктор\n";
                ParameterInfo[] parametrs = constructor.GetParameters();
                foreach (ParameterInfo parametr in parametrs)
                {
                    s = s + parametr.ParameterType.Name + " " + parametr.Name + ", ";
                }
                s = s.Trim();
                s = s.TrimEnd(',');
                tag += "Входные параметры: " + s;
                node1 = new TreeNode();
                node1.Text = node.Text + "(" + s + ")";
                node1.ImageIndex = 6;
                node1.SelectedImageIndex = 6;          
                node1.Tag = tag;
                node.Nodes.Add(node1);
            }
            //Загрузить методы
            foreach (MethodInfo method in methodsAll)
            {
                string s = "", tag = "";
                ParameterInfo[] parametrs = method.GetParameters();
                foreach (ParameterInfo parametr in parametrs)
                {
                    s = s + parametr.ParameterType.Name + ", ";
                }
                s = s.Trim();
                s = s.TrimEnd(',');
                node1 = new TreeNode();
                node1.Text = method.ReturnType.Name + " " + method.Name + "("
                             + s + ")  [";

                tag += "Модификатор доступа:";
                if (method.IsPublic)
                {
                    tag += "public";
                    node1.ImageIndex = 7;
                    node1.SelectedImageIndex = 7;
                }
                else if (method.IsPrivate)
                {
                    tag += "private";
                    node1.ImageIndex = 9;
                    node1.SelectedImageIndex = 9;
                }
                else
                {
                    tag += "other";
                    node1.ImageIndex = 8;
                    node1.SelectedImageIndex = 8;
                }

                tag += "\n";
                tag += "Входные параметры: " + s + "\n";
                tag += "Возращаемый тип: " + method.ReturnType.Name + "\n";
                tag += "Attr: " + method.Attributes + "\n";
                tag += "Атрибуты: ";

                var attr = method.CustomAttributes;
                foreach (CustomAttributeData date in attr)
                {
                    tag += date.AttributeType.FullName;

                    node1.Text += date.AttributeType.FullName + ", ";
                }
                node1.Text += "]";

                node1.Tag = tag;
                node.Nodes.Add(node1);
            }
        }

        private void открытьСборкуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            treeView1.Nodes.Clear();
            string path = selectAssemblyFile();
            if (path != null)
            {
                assembly = openAssembly(path);
            }
            if (assembly != null)
            {
                TreeNode root = new TreeNode();
                root.Text = assembly.GetName().Name;
                root.ImageIndex = 0;
                root.SelectedImageIndex = 0;
                treeView1.Nodes.Add(root);
                Type[] types = assembly.GetTypes();
                addRoot(root, types);
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            string tag = "\nИнформация:\n";
            if (e.Node.Tag != null)
            {
                tag += e.Node.Tag;
            }
            richTextBox1.Text = e.Node + tag;
        }
    }
}